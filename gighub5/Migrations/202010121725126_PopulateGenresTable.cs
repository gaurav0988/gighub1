namespace gighub5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGenresTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Genres (Id, Name) Values (1,'Gaurav')");
            Sql("INSERT INTO Genres (Id, Name) Values (2,'Rahul')");
            Sql("INSERT INTO Genres (Id, Name) Values (3,'Gagan')");
            Sql("INSERT INTO Genres (Id, Name) Values (4,'Raghav')");
           
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Genres WHERE Id IN (1,2,3,4)");
        }
    }
}
